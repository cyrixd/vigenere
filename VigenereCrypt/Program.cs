﻿using System;

namespace VigenereCrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            string key = "zsxdcfvgbhnjm";

            Console.WriteLine("String for Encoding:");
            string[] source = { "asdfghjkl;'", "`1234567890-="};
            foreach (var t in source)
            {
                Console.WriteLine(t);
            }

            Console.WriteLine("Encoded strings:");
            foreach (var t in VigenereCrypt.Encrypt(source, key))
            {
                Console.WriteLine(t);
            }

            Console.WriteLine("Decoded strings:");
            foreach (var t in VigenereCrypt.Decrypt(VigenereCrypt.Encrypt(source, key), key))
            {
                Console.WriteLine(t);
            }

            Console.ReadKey();
        }
    }
}
