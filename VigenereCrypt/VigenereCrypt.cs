﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VigenereCrypt
{
    public class VigenereCrypt
    {
        public static IEnumerable<string> Encrypt(IEnumerable<string> source, string key)
        {
            source = source.Take(UInt16.MaxValue);

            foreach (string sourceString in source)
            {
                var qw = key.Length;
                if (key.Length == 0)
                {
                    yield return sourceString;
                    continue;
                }

                byte[] sourceByte = Encoding.Unicode.GetBytes(sourceString);
                byte[] keyByte = Encoding.Unicode.GetBytes(key);
                byte[] encodedBytes = new byte[sourceByte.Length];


                for (int i = 0; i < sourceByte.Length; i += 2)
                {
                    ushort numericCurrentSourceChar = BitConverter.ToUInt16(sourceByte, i);
                    ushort numericCurrentKeyChar = BitConverter.ToUInt16(keyByte, (i + keyByte.Length) % keyByte.Length);

                    ushort tmp = (ushort)((numericCurrentSourceChar + numericCurrentKeyChar) % (ushort.MaxValue + 1));

                    encodedBytes[i] = (byte)(tmp % (byte.MaxValue + 1));
                    encodedBytes[i + 1] = (byte)(tmp / (byte.MaxValue + 1));
                }

                yield return Encoding.Unicode.GetString(encodedBytes);
            }
            yield break;
        }

        public static IEnumerable<string> Decrypt(IEnumerable<string> encrypted, string key)
        {
            foreach (string encryptedString in encrypted)
            {
                if (key.Length == 0)
                {
                    yield return encryptedString;
                    continue;
                }
                encrypted = encrypted.Take(UInt16.MaxValue);

                byte[] encryptedByte = Encoding.Unicode.GetBytes(encryptedString);
                byte[] keyByte = Encoding.Unicode.GetBytes(key);
                byte[] decodedBytes = new byte[encryptedByte.Length];


                for (int i = 0; i < encryptedByte.Length; i += 2)
                {
                    ushort numericCurrentEncryptedChar = BitConverter.ToUInt16(encryptedByte, i);
                    ushort numericCurrentKeyChar = BitConverter.ToUInt16(keyByte, (i + keyByte.Length) % keyByte.Length);

                    ushort tmp = (ushort)((numericCurrentEncryptedChar - numericCurrentKeyChar + ushort.MaxValue + 1) % (ushort.MaxValue + 1));

                    decodedBytes[i] = (byte)(tmp % (byte.MaxValue + 1));
                    decodedBytes[i + 1] = (byte)(tmp / (byte.MaxValue + 1));
                }

                yield return Encoding.Unicode.GetString(decodedBytes);
            }
            yield break;
        }
    }
}
