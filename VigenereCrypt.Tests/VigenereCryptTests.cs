﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VigenereCrypt.Tests
{
    [TestClass]
    public class VigenereCryptTests
    {
        [TestMethod]
        public void EncryptTest()
        {
            string[] source = { "AAAAA", "ZZZZZ" };
            string key = " ";
            string[] expectedEncrypt = { "aaaaa", "zzzzz" };

            string[] actualEncrypt = VigenereCrypt.Encrypt(source, key).ToArray();

            CollectionAssert.AreEqual(expectedEncrypt, actualEncrypt);
        }

        [TestMethod]
        public void EncryptTestKeyEmpty()
        {
            string[] source = { "AAAAA", "ZZZZZ" };
            string key = String.Empty;

            string[] actualEncrypt = VigenereCrypt.Encrypt(source, key).ToArray();

            CollectionAssert.AreEqual(source, actualEncrypt);
        }

        [TestMethod]
        public void DecryptTest()
        {
            string[] encrypt = { "eeeee", "xxxxx" };
            string key = " ";
            string[] expectedDecrypt = { "EEEEE", "XXXXX" };

            string[] actualDecrypt = VigenereCrypt.Decrypt(encrypt, key).ToArray();

            CollectionAssert.AreEqual(expectedDecrypt, actualDecrypt);
        }

        [TestMethod]
        public void DecryptTestKeyEmpty()
        {
            string[] encrypt = { "eeeee", "xxxxx" };
            string key = String.Empty;

            string[] actualDecrypt = VigenereCrypt.Decrypt(encrypt, key).ToArray();

            CollectionAssert.AreEqual(encrypt, actualDecrypt);
        }

        [TestMethod]
        public void LoopTest()
        {
            string[] expected = { "Qwertyuiop1234567890", "АбырвалГ" };
            string key = "C#";

            string[] actualLoppEncryptDecrypt = VigenereCrypt.Decrypt(VigenereCrypt.Encrypt(expected, key), key).ToArray();

            CollectionAssert.AreEqual(expected, actualLoppEncryptDecrypt);
        }
    }
}
